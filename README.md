
# login to gitlab
docker login registry.gitlab.com

# build and push image registry.gitlab.com/stroov/<project-name>
docker build -t registry.gitlab.com/stroov/vue-test .
docker push registry.gitlab.com/stroov/vue-test

# pull image
docker pull registry.gitlab.com/stroov/vue-test
